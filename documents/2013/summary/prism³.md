# French version 🇫🇷

29/06/2013

Dans le document [20130629-thewashingtonpost__prism.pdf](../files/20130629-thewashingtonpost__prism.pdf), on apprend

* Le processus d'analyse et de collection des données (voir le document pour plus d'informations)
* Le type de données collectées par le programme **PRISM**
🔴 Les recherches sur internet (Google, Yahoo, Microsoft)
🔴 Les chats (Skype, PalTalk, Microsoft, Hangouts, Facebook)
🔴 Les connexions et déconnexions aux services de mail/chat (Microsoft, Yahoo, Google, Facebook, PalTalk, Skype, AOL)
🔴 Le contenu et les métadonnées des emails (Microsoft, Yahoo, Google, AOL)
🔴 VoIP (AOL, peut-être d'autres)
🔴 Forums
🔴 Photos, messages, posts, informations d'identification (nom, prénom, mot de passe, email) sur les réseaux sociaux (Microsoft, Google, Facebook, YouTube)
🔴 Vidéo (Skype, PalTalk, Microsoft)


# English version 🇺🇸

06/29/2013

In the document [20130629-thewashingtonpost__prism.pdf](../files/20130629-thewashingtonpost__prism.pdf), we learn

* The process of data analysis and collection (see the document for more information)
* The type of data collected by the **PRISM** program
🔴 Internet searches (Google, Yahoo, Microsoft)
🔴 Cats (Skype, PalTalk, Microsoft, Hangouts, Facebook)
🔴 Connections and disconnections to mail / chat services (Microsoft, Yahoo, Google, Facebook, PalTalk, Skype, AOL)
🔴 The content and metadata of emails (Microsoft, Yahoo, Google, AOL)
🔴 VoIP (AOL, maybe others)
🔴 Forums
🔴 Photos, messages, posts, identification information (name, first name, password, email) on social networks (Microsoft, Google, Facebook, YouTube)
🔴 Video (Skype, PalTalk, Microsoft)
