# French version 🇫🇷

06/06/2013

Dans le document [20130606-thewashingtonpost__prism.pdf](../files/20130606-thewashingtonpost__prism.pdf), on apprend que les entreprises comme :
   🔴 Microsoft
   🔴 Google
   🔴 Yahoo!
   🔴 Facebook
   🔴 PalTalk
   🔴 YouTube
   🔴 Skype
   🔴 AOL
   🔴 Apple

Fournissent les informations suivantes aux services secrets américains :

- Le contenu des Emails

- Le contenu des Chats - vidéos, audios

- Les Vidéos / Photos

- Les données enregistrées

- Les appels en VoIP

- Les fichiers transférés

- Les données des conférences vidéos

- Les notifications d'activité sur les comptes (Connexion)

- Requêtes spéciales (on ne sait pas ce que c'est...)

	

# English version 🇺🇸

06/06/2013

In the document [20130606-thewashingtonpost__prism.pdf](../files/20130606-thewashingtonpost__prism.pdf), we learn that companies like:
    🔴 Microsoft
    🔴 Google
    🔴 Yahoo!
    🔴 Facebook
    🔴 PalTalk
    🔴 YouTube
    🔴 Skype
    🔴 AOL
    🔴 Apple

Provide the following information to the U.S. Secret Service:

- The content of Emails
- Chats content - videos, audios
- Videos / Photos
- Recorded data
- VoIP calls
- Transferred files
- Video conference data
- Activity notifications on accounts (Connection)
- Special requests (we don't know what it is ...)